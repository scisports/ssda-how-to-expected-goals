# Tech how-to: Build your own expected-goals model for football
This repository contains a tech how-to on building an expected-goals model for football. More specifically, this repository contains:

* a Jupyter notebook explaining the data science process;

* an artificial but realistic shots database covering 127,643 shots.

## Install
This section provides instructions on setting up the environment for running the notebook and using the data in this repository.

1. Install Anaconda 5.1 for Python version 3.6 by following the instructions on the [official website](https://www.anaconda.com/download/).

2. Install Git by following the instructions on the [official website](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git). 

3. Download this repository from the [downloads page](https://bitbucket.org/scisports/ssda-how-to-expected-goals/downloads/).

4. Extract the archive file to your local disk.

5. Create a Conda environment from the `environment.yml` file in this repository by running the following command:

        conda env create -f environment.yml

    The above command will create a Conda environment called `ssda-how-to-expected-goals` and install the required Python modules in that environment. The Conda environment will be available as a kernel for Jupyter notebook.


## Run
This section provides instructions for running the Jupyter notebook and using the data in this repository.

1. Start a Jupyter notebook server by running the following command:

        jupyter notebook 

2. Navigate to the `notebooks` folder and launch the `how-to-expected-goals.ipynb` notebook.

3. Follow the steps in the notebook.

## Author
Contact Jan Van Haaren (<j.vanhaaren@scisports.com>) if you would have any questions, comments or remarks regarding this tech how-to.

## Acknowledgements
Special thanks to the following people for their help:

* Lotte Bransen for providing feedback on early versions of this how-to;

* Robert Seidl for providing feedback on the `environment.yml` file.
